<?php
require_once '../twig/vendor/autoload.php';

$loader = new Twig_Loader_Filesystem('./twig');
$twig = new Twig_Environment($loader, array(
//    'cache' => './compilation_cache',
));

if (!isset($_GET['url'])&&!isset($_GET['chosenURL'])) {
  echo $twig->render('oppgave1.html', array ('url'=>''));
} else if (!isset($_GET['chosenURL'])) {
  $feeds = findFeeds ($_GET['url']);
  echo $twig->render('oppgave1.html', array ('url'=>$_GET['url'], 'feeds'=>$feeds));
} else {
  $feeds = findFeeds ($_GET['url']);
  $news = findNews ($_GET['chosenURL']);
  echo $twig->render('oppgave1.html', array ('url'=>$_GET['url'],
                                             'source'=>$_GET['chosenURL'],
                                             'feeds'=>$feeds, 'news'=>$news));
}

function findFeeds ($url) {
  $doc = new DOMDocument();
  @$doc->loadHTMLFile($url);  // Load html document, suppress warnings
  $xpath = new DOMXPath($doc);

  $elements = $xpath->query("//link[@rel='alternate' and @type='application/rss+xml']");
  $data = [];
  if (!is_null($elements)) {
    foreach ($elements as $element) {
      $data[] = array('title'=>$element->getAttribute('title'), 'href'=>$element->getAttribute('href'));
    }
  }
  return $data;
}


function findNews($url) {
  $raw = file_get_contents ($url);
  $xml=simplexml_load_string($raw) or die("Error: Cannot create object");

  $data = [];
  foreach ($xml->channel->item as $item) {
    $data[] = array('title'=>(string)$item->title, 'descr'=>(string)$item->description, 'url'=>(string)$item->link, 'img'=>(string)$item->image);
  }
  return $data;
}
