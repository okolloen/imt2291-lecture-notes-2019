<?php
require_once '../twig/vendor/autoload.php';
require_once "Pinterest.php";

$loader = new Twig_Loader_Filesystem('./twig_templates');
$twig = new Twig_Environment($loader, array(
    //'cache' => './compilation_cache',
));

if (isset($_GET['q'])) {  // Allow user to override default query
  $q = $_GET['q'];
} else {                  // If no query given, use default
  $q = "mathematical riddles fun";
}

$pins = Pinterest::getPinsWithURLS($q);
$data['search'] = $q;     // Showing the query executed on the page
$data['pins'] = $pins;

echo $twig->render('oppgave5.html', $data);
