<?php
/**
 * This class is for adding, listing, editing and searching contacts in
 * the contacts database.
 */
class Contacts {
  private $dsn = 'mysql:dbname=contact_registry;host=127.0.0.1';
  private $user = 'root';
  private $password = '';
  private $db = null;

  /**
   * Connect to the database when object is created.
   */
  public function __construct() {
    try {
        $this->db = new PDO($this->dsn, $this->user, $this->password);
    } catch (PDOException $e) {
        // NOTE IKKE BRUK DETTE I PRODUKSJON
        echo 'Connection failed: ' . $e->getMessage();
    }
  }

  public function __destruct() {
    if ($this->db!=null) {
      unset ($this->db);
    }
  }

  /**
   * Adds a contact to the database.
   * @param array with 'givenName', 'familyName', 'phone' and 'email'.
   * @return an array with only element 'status'=='OK' on success.
   *        'status'=='FAIL' on error, the error info can be found
   *        in 'errorInfo'.
   */
  public function addContact ($data) {
    $sql = 'insert into contact (givenName, familyName, email, phone) VALUES (?, ?, ?, ?)';
    $sth = $this->db->prepare ($sql);
    $sth->execute (array ($data['givenName'], $data['familyName'],
                          $data['email'], $data['phone']));
    $tmp = [];
    if ($sth->rowCount()==1) {
      $tmp['status'] = 'OK';
      $tmp['id'] = $this->db->lastInsertId();
    } else {
      $tmp['status'] = 'FAIL';
      $tmp['errorMessage'] = 'Failed to insert into contact registry';
      $tmp['errorInfo'] = $sth->errorInfo();
    }
    return $tmp;
  }

  /**
   * Return a list of all the contacts in the database.
   *
   * @return array with the element 'status' set to 'OK' on success, 'FAIL' on failure.
   *        The element 'contacts' is an array with all contacts in the database sorted on familyName, then givenName.
   *        Each element in the array contains the id, givenName, familyName,
   *        email and phone number for the contact.
   */
  public function listContacts() {
    $sql = 'SELECT id, givenName, familyName, email, phone FROM contact ORDER BY familyName, givenName';
    $sth = $this->db->prepare ($sql);
    $sth->execute(array());
    if ($sth->errorInfo()[0]=='00000') {
      $data['status'] = 'OK';
      $data['contacts'] = $sth->fetchAll(PDO::FETCH_ASSOC);
    } else {
      $data['status'] = 'FAIL';
      $data['errorMessage'] = 'Failed to retrieve contacts from contact registry';
      $data['errorInfo'] = $sth->errorInfo();
    }
    return $data;
  }

  /**
   * Searches the database for contacts matching the given string.
   * GivenName and familyName will be concatenated and partial hits will also
   * be returned.
   *
   * @param name a string with a name to search for.
   * @return array with the element 'status' set to 'OK' on success, 'FAIL' on failure.
   *        On success the 'contacts'  element is an array with all matching contacts. The information returned form
   *        each contact will be id, givenName, familyName, phone and email.
   *        The 'search' element contains the string that was searched for.
   */
  public function searchContacts ($name) {
    $sql = 'SELECT id, givenName, familyName, email, phone '.
           'FROM contact '.
           'WHERE CONCAT(givenName, " ", familyName) like ? '.
           'ORDER BY familyName, givenName ';
    $sth = $this->db->prepare ($sql);
    $sth->execute(array("%$name%"));
    if ($sth->errorInfo()[0]=='00000') {
      $data['status'] = 'OK';
      $data['contacts'] = $sth->fetchAll(PDO::FETCH_ASSOC);
      $data['search'] = $name;
    } else {
      $data['status'] = 'FAIL';
      $data['errorMessage'] = 'Failed to retrieve contacts from contact registry';
      $data['errorInfo'] = $sth->errorInfo();
    }
    return $data;
  }

  /**
   * Get all information about contact with given ID.
   *
   * @param id the id of the contact to get information about.
   * @return array with the element 'status' set to 'OK' on success, 'FAIL' on failure.
   *              On success the information about the contact is found in the element 'contact'.
   *              On failure more information is given in 'errorMessage'.
   */
  public function getContact ($id) {
    $sql = 'SELECT id, givenName, familyName, email, phone FROM contact WHERE id=?';
    $sth = $this->db->prepare ($sql);
    $sth->execute(array($id));
    if ($sth->errorInfo()[0]=='00000') {
      if ($row = $sth->fetch(PDO::FETCH_ASSOC)) {
        $data['status'] = 'OK';
        $data['contact'] = $row;
      } else {
        $data['status'] = 'FAIL';
        $data['errorMessage'] = 'Contact not found';
      }
    } else {
      $data['status'] = 'FAIL';
      $data['errorMessage'] = 'Failed to retrieve contact from contact registry';
      $data['errorInfo'] = $sth->errorInfo();
    }
    return $data;
  }

  /**
   * Remove the contact with the given ID.
   * @param id the ID of the contact to remove.
   * @return array with the element 'status' set to 'OK' on success, 'FAIL' on failure.
   */
   public function deleteContact ($id) {
     $sql = 'DELETE FROM contact WHERE id=?';
     $sth = $this->db->prepare ($sql);
     $sth->execute(array($id));
     if ($sth->rowCount()==1) {
       $tmp['status'] = 'OK';
     } else {
       $tmp['status'] = 'FAIL';
       $tmp['errorMessage'] = 'No contact with that ID is registered';
     }
     return $tmp;
   }

  /**
   * Updates a contact in the database.
   * @param data an assosiative array with id, givenName, familyName, phone and email
   *         of the contact to be updated.
   * @return array with the element 'status' set to 'OK' on success, 'FAIL' on failure.
   *        on success the element 'contact' contains the same data as was sent in with ''$data'
   */
  public function updateContact($data) {
    $sql = 'UPDATE contact '.
           'SET givenName=?, familyName=?, phone=?, email=? '.
           'WHERE id=?';

    $sth = $this->db->prepare ($sql);
    $sth->execute (array ($data['givenName'], $data['familyName'],
                          $data['phone'], $data['email'], $data['id']));
    $tmp = [];
    if ($sth->rowCount()==1) {
      $tmp['status'] = 'OK';
      $tmp['contact'] = $data;
    } else {
      $tmp['status'] = 'FAIL';
      $tmp['errorMessage'] = 'Failed to insert into contact registry';
      $tmp['errorInfo'] = $sth->errorInfo();
    }
    return $tmp;
  }
}
