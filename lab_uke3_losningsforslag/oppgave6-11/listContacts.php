<?php

require_once '../../twig/vendor/autoload.php';
require_once "classes/Contacts.php";

$loader = new Twig_Loader_Filesystem('./twig_templates');
$twig = new Twig_Environment($loader, array(
    //'cache' => './compilation_cache',
));

$contacts = new Contacts();
$res = $contacts->listContacts ();

echo $twig->render('listContacts.html', $res);
