<?php
require_once '../twig/vendor/autoload.php';
require_once "Pinterest.php";

$loader = new Twig_Loader_Filesystem('./twig_templates');
$twig = new Twig_Environment($loader, array(
    //'cache' => './compilation_cache',
));

if (isset($_GET['q'])) {
  $q = $_GET['q'];
} else {
  $q = "mathematical riddles fun";
}
$pins = Pinterest::getPins($q);
$data['search'] = $q;
$data['pins'] = $pins;

echo $twig->render('oppgave3.html', $data);
