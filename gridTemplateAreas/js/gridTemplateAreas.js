$(function(){
  $("textarea").change(function(){
    let lines = this.value.split("\n");
    let id = "";
    let css = "";
    for (let i=0; i<lines.length; i++) {
      if (lines[i].trim().startsWith(".")) {
        id = lines[i];
        id = id.trim();
        id = id.substring(0, id.length-1);
        id = id.trim();
        css = "";
      } else if (lines[i].trim().startsWith("}")) {
        document.querySelector(id).style.cssText = css;
      } else {
        css += lines[i].trim();
      }
    }
    $("textarea").focus();
  });
});
