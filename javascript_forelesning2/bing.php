<?php

// Denne koden kommer fra : https://docs.microsoft.com/en-us/azure/cognitive-services/bing-image-search/quickstarts/php

// NOTE: Be sure to uncomment the following line in your php.ini file.
// ;extension=php_openssl.dll

// **********************************************
// *** Update or verify the following values. ***
// **********************************************

// Replace the accessKey string value with your valid access key.
$accessKey = 'd765744d011340e0949132d6831ff89e';


// Verify the endpoint URI.  At this writing, only one endpoint is used for Bing
// search APIs.  In the future, regional endpoints may be available.  If you
// encounter unexpected authorization errors, double-check this value against
// the endpoint for your Bing Search instance in your Azure dashboard.
$endpoint = 'https://api.cognitive.microsoft.com/bing/v7.0/images/search';

$term = 'puppies';

function BingImageSearch ($url, $key, $query) {
  /*  $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url . "?q=" . urlencode($query));
    $headers = ["Ocp-Apim-Subscription-Key: $key"];
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    $server_output = curl_exec ($ch);

    curl_close ($ch);
    return array("", $server_output);*/

    // Prepare HTTP request
    // NOTE: Use the key 'http' even if you are making an HTTPS request. See:
    // http://php.net/manual/en/function.stream-context-create.php
    $headers = "Ocp-Apim-Subscription-Key: $key\r\n";
    $options = array ( 'http' => array (
                           'header' => $headers,
                           'method' => 'GET' ));

    // Perform the Web request and get the JSON response
    $context = stream_context_create($options);
    $result = file_get_contents($url . "?q=" . urlencode($query), false, $context);

    // Extract Bing HTTP headers
    $headers = array();
    foreach ($http_response_header as $k => $v) {
        $h = explode(":", $v, 2);
        if (isset($h[1]))
            if (preg_match("/^BingAPIs-/", $h[0]) || preg_match("/^X-MSEdge-/", $h[0]))
                $headers[trim($h[0])] = trim($h[1]);
    }

    return array($headers, $result);
}

if (strlen($accessKey) == 32) {

    //print "Searching images for: " . $term . "\n";

    list($headers, $json) = BingImageSearch($endpoint, $accessKey, $term);

    //print "\nRelevant Headers:\n\n";
    foreach ($headers as $k => $v) {
      //  print $k . ": " . $v . "\n";
    }

    //print "\nJSON Response:\n\n<pre>";
    //echo json_encode(json_decode($json), JSON_PRETTY_PRINT);
    echo $json;

} else {

    print("Invalid Bing Search API subscription key!\n");
    print("Please paste yours into the source code.\n");

}
?>
