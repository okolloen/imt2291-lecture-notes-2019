'use strict';   // Default i moduler
import Slideshow from './lib/Slideshow.js';

function createSlideshow (id, theme='', images) {
  const slideshow = new Slideshow(id, theme, images);
}

window.createSlideshow = createSlideshow;
