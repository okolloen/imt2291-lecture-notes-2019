import {qs as $} from './util.js';

export default class Slideshow {
  constructor (id, theme='', images) {
    this.id = id;
    $(id).innerHTML = '<div></div><div></div>';  // Add two divs into the div
    this.images = [];
    this.currentImage = 1;
    /* MS har endret vilkårene for søking med bing, dvs at bing.php ikke lenger virker
    fetch (`bing.php?theme=${theme}`)
    .then(res=>res.json())
    .then(json=> {
      json.value.forEach(img => {
        this.images.push(img.thumbnailUrl);
      });
      $(`${id} div:first-child`).style.backgroundImage = `url(${this.images[0]})`;
    });*/
    this.images = images;
    $(`${id} div:first-child`).style.backgroundImage = `url(${this.images[0]})`;

    $(`${id} div:last-child`).addEventListener('transitionend', e=>{
      $(`${id} div:last-child`).style.opacity = 0;
      $(`${id} div:first-child`).style.backgroundImage =
                      $(`${id} div:last-child`).style.backgroundImage;
    });

    window.setTimeout(this.slideShow.bind(this), 5000);  // bind this i slideShow til this
  }

  loadImage(url) {
    return new Promise((resolve, reject)=>{
      const img = document.createElement('img');
      img.onload = resolve;
      img.src = url;
    });
  }

  slideShow() {
    this.currentImage++;
    if (this.currentImage==this.images.length) {  // Dersom vi har kommet til slutten så går vi til første bilde igjen
      this.currentImage=0;
    }
    this.loadImage(this.images[this.currentImage]).then (e=>{
      $(`${this.id} div:last-child`).style.backgroundImage = `url(${e.target.src})`;  // Bytt bilde i #img2, vil når det er ferdig lastet bli fadet inn
      $(`${this.id} div:last-child`).style.opacity = 1;
    });
    window.setTimeout(this.slideShow.bind(this), 5000);                      // Kall på nytt om fem sekunder
  }
}
