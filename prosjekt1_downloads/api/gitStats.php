<?php
$repo = $_GET['repo'];
$baseFolder = $_GET['baseFolder'];

chdir ($baseFolder);
chdir ($repo);

$data = [];

$tmp = []; // Commits, with merges, https://stackoverflow.com/a/9839491
exec ('git shortlog -s -n --all', $tmp);

$tmp1 = []; // Commits, without merges, https://stackoverflow.com/a/9839491
exec ('git shortlog -s -n --all --no-merges', $tmp1);

$noMergeCommits = [];
for ($i=0; $i<count($tmp1); $i++) {
  if (count(explode ("\t", trim($tmp1[$i])))>=2) {
    list ($commits, $user) = explode ("\t", trim($tmp1[$i]));
    $noMergeCommits[$user] = $commits;
  }
}

for ($i=0; $i<count($tmp); $i++) {  // Go through all users with commits
  $stat = [];
  if (count(explode ("\t", trim($tmp[$i])))>=2) {
    list ($commits, $user) = explode ("\t", trim($tmp[$i]), 2);

    // Get lines added, deleted for each user
    // From : https://gist.github.com/sephiroth74/d3ce654dcaefe1d2c4cd
    $cmd = 'git log --all --numstat --pretty="%H" --author="'.$user.'" --since=1.year | awk '."'NF==3 {plus+=$1; minus+=$2} NF==1 {total++} END {printf(".'"lines added: +%d\nlines deleted: -%d\n", plus, minus)}'."'";

    exec ($cmd, $stat);
    list ($added, $deleted) = $stat;
    $added = trim(explode(":", $added)[1])*1;
    $deleted = trim(explode(":", $deleted)[1])*1;
    $data[] = array('user'=>$user,
                    'commits'=>$commits,
                    'merges'=>isset($noMergeCommits[$user])?$commits-$noMergeCommits[$user]:$commits,
                    'linesAdded'=>$added,
                    'linesDeleted'=>$deleted);
  }
}

header ("Content-type: application/json");
echo json_encode($data);
