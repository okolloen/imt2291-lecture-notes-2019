<?php
require_once './classes/DB.php';
require_once './classes/User.php';

class UserTest extends \Codeception\Test\Unit {
  /**
   * @var \UnitTester
   */
  protected $tester;
  private $familyName, $givenName, $userData;
  private $pwd = 'mySecretPassword';
  private $user;
  private $db;

  protected function _before() {
    $db = DB::getDBConnection();
    $this->familyName = md5(date('l jS \of F Y h:i:s A'));  // Create random 32 character string
    $this->givenName = md5(date('l jS h:i:s A \of F Y '));  // Create random 32 character string
    $this->userData['givenName'] = $this->givenName;
    $this->userData['familyName'] = $this->familyName;
    $this->userData['uname'] = $this->givenName.'@'.$this->familyName.'.test';
    $this->userData['pwd'] = $this->pwd;

    $this->user = new User($db);
  }

  protected function _after() {
  }

  /**
   * Checks if it is possible through an object of the User class to add a
   * new user to the user table in the database.
   */
  public function testCanCreateUser() {
    $data = $this->user->addUser($this->userData);
    $this->assertEquals('OK', $data['status'], 'Failed to create new user.');
    $this->assertTrue($data['id']>0, 'ID of new user should be > 0.');

    // Delete user
    $deleted = $this->user->deleteUser($data['id']);
    $this->assertEquals('OK', $deleted['status'], 'User could not be deleted.');
  }

  /**
   * Checks if it possible to log in to a newly created user instance.
   */
  public function testCanDoLogin() {
    $data = $this->user->addUser($this->userData);
    $loginstatus = $this->user->login($this->userData['uname'], $this->pwd);
    $this->assertEquals('OK', $loginstatus['status'], 'Failed to log in.');

    // Check that session id is set correctly
    $this->assertEquals($data['id'], $_SESSION['uid'], 'Bad/missing session UID');
    // Delete user
    $deleted = $this->user->deleteUser($data['id']);

    // This login should fail
    $loginstatus = $this->user->login($this->userData['uname'], $this->pwd);
    $this->assertEquals('FAIL', $loginstatus['status'], 'Failed to fail to log in.');
  }
}
