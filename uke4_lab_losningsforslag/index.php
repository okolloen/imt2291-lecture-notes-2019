<?php
session_start();

$test = 2;
require_once "classes/User.php";
require_once "classes/DB.php";
require_once '../twig/vendor/autoload.php';

$loader = new Twig_Loader_Filesystem('./templates');
$twig = new Twig_Environment($loader, array(
//    'cache' => './compilation_cache',
));

$db = DB::getDBConnection();

if ($db==null) {
  echo $twig->render('error.html', array('msg' => 'Unable to connect to the database!'));
  die();  // Abort further execution of the script
}

$user = new User($db);

if ($user->loggedIn()) {
  echo $twig->render('index.html', array('loggedin'=>'yes')); // Add other data as needed
} else {
  echo $twig->render('index.html', array());  // Add data as needed
}
