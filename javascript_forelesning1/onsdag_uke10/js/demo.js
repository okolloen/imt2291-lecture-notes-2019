window.addEventListener('load', init);

function init() {

  const knappen = document.getElementById('knapp');
  console.log (knappen);

  const arr = [1, 2, 3, 4, "hei", "på", "deg"];
  const arr1 = Array(3,1); // NO NO
  console.log (arr, arr1);

  const obj = {
    var1: "hei",
    var2: 42
  }

  console.log (obj);
  console.log (obj.var1);


  var obj1 = new KlasseDefinisjon("Øivind");
  obj1.hentJSON("data.json");
  console.log ("her", obj1);

  function KlasseDefinisjon (param1) {
    this.var3 = param1;
    this.test = "Hallo ";

    this.siHei = function() {
      return this.test+this.var3;
    }

    this.hentJSON = function(fil) {
      const req = new XMLHttpRequest();

      req.onreadystatechange = this.noeHendte;
      req.onload = this.ferdig.bind(this);
      req.open('get', fil, true);
      req.send();
    }

    this.noeHendte = function (e) {
      console.log (this, e);
    }

    this.ferdig = function (e) {
      console.log ("ferdig", this, e);
      this.data = JSON.parse(e.target.responseText);
    }
  }


}

Number.prototype.pad = function (len, char="0") {
  var n = this.toString();
  return Array(len-n.length).fill(char).join("")+n;
}

let num = 4;

console.log (num.pad(8, "#"));
