<?php
if (!isset($_COOKIE['priorVisits'])) {
  setcookie('priorVisits', 1, time()+60*60*24 /*60 seconds*60minutes*24hours */
                            /* [, string $path = "" [, string $domain = ""
                               [, bool $secure = false
                               [, bool $httponly = false ]]]]*/);
  $message = "Velkommen førstegangsbesøkende";
} else {
  setcookie('priorVisits', $_COOKIE['priorVisits']+1, time()+60*60*24);
  $message = "Velkomment til ditt {$_COOKIE['priorVisits']} besøk her.";
}

if (isset($_GET['deletecookie'])) {
  setcookie('priorVisits', '', time()-3600); // blank and expires one hour ago
  $message = "Jeg glemmer at du noen gang har vært her.";
}

?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Småkaker</title>
</head>
<body>
  <?php
    echo "<h1>$message</h1>";
   ?>
   <a href="index.php?deletecookie=1">Glem meg</a> | <a href="index.php">Prøv igjen</a>
</body>
</html>
