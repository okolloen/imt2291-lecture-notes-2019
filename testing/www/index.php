<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Postal code/area search system</title>
</head>
<body>
  <form id="searchForm" action="index.php" method="get">
    <label for="postal">Postal code or place: </label>
    <input id="postal" name="postal"/>
    <input type="submit" value="Search"/>
  </form>
  <?php
    if (isset($_GET['postal'])) {
      require_once ("classes/PostalSearch.php");

      $postal = $_GET['postal'];
      if (is_numeric($postal)) {
        $res = PostalSearch::postalCodeSearch($postal);
      } else {
        $res = PostalSearch::placeNameSearch($postal);
      }
      echo '<pre>';
      print_r ($res);
      echo '</pre>';
      echo '<div id="result">';
      foreach ($res as $postinfo) {
        echo "{$postinfo->postalCode} {$postinfo->placeName}, {$postinfo->adminName2}</br>\n";
      }
      echo '</div>';
    }
   ?>
</body>
</html>
