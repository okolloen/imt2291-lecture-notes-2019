<?php
require_once '../classes/DB.php';

$db = DB::getDBConnection();

if (is_uploaded_file($_FILES['fileToUpload']['tmp_name'])) {
  $content = file_get_contents($_FILES['fileToUpload']['tmp_name']);
  $mime = $_FILES['fileToUpload']['type'];
  $name = $_FILES['fileToUpload']['name'];
  $size = $_FILES['fileToUpload']['size'];
  $sql = "INSERT INTO filesInDB (owner, name, mime, size, description, content) VALUES (1, :name, :mime, :size, :description, :content)";
  $sth = $db->prepare ($sql);
  $sth->bindParam(':name', $name);
  $sth->bindParam(':mime', $mime);
  $sth->bindParam(':size', $size);
  $sth->bindParam(':description', $_POST['descr']);
  $sth->bindParam(':content', $content);
  $sth->execute ();
  if ($sth->rowCount()==1) {
    echo "<script>\nwindow.parent.success();\n</script>\n";
  } else {
    echo "<script>\nwindow.parent.fail();\n</script>\n";
  }
} else {  // Some trickery is going on
  echo "<script>\nwindow.parent.bad();\n</script>\n";
}
