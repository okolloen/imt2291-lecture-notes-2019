<?php
require_once '../../twig/vendor/autoload.php';
require_once '../classes/DB.php';

$loader = new Twig_Loader_Filesystem('./twig');
$twig = new Twig_Environment($loader, array(
//    'cache' => './compilation_cache',
));

echo $twig->render('upload.html', array());
