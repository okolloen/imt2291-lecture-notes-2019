<?php
require_once '../../twig/vendor/autoload.php';
require_once '../classes/DB.php';

$loader = new Twig_Loader_Filesystem('./twig');
$twig = new Twig_Environment($loader, array(
//    'cache' => './compilation_cache',
));

$db = DB::getDBConnection();

$sql = 'SELECT id, name, size, description FROM filesInDB ORDER BY name';
$sth = $db->prepare ($sql);
$sth->execute();

$result = $sth->fetchAll(PDO::FETCH_ASSOC);

echo $twig->render('index.html', array ('files'=>$result));
