<h1>Artikler i VG akkurat nå</h1>
<pre>
<?php
$doc = new DOMDocument();
@$doc->loadHTMLFile('https://www.vg.no/');  // Load html document, suppress warnings
$xpath = new DOMXPath($doc);

// Find div tags with given class, then first a tag within a span within the first h3 tag
$elements = $xpath->query("//div[@class='article-content']/h3[1]/span/a[1]");
if (!is_null($elements)) {
  foreach ($elements as $element) {
    if ($element->getAttribute('title')!='') {  // If title attribute is not blank, write it
      echo $element->getAttribute('title'). "\n";
    }
  }
}
