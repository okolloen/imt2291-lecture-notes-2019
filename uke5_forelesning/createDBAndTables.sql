--
-- Database: `fileStorage`
--
CREATE DATABASE IF NOT EXISTS `fileStorage` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `fileStorage`;

-- --------------------------------------------------------

--
-- Table structure for table `filesInDB`
--

DROP TABLE IF EXISTS `filesInDB`;
CREATE TABLE `filesInDB` (
  `id` bigint(20) NOT NULL,
  `owner` bigint(20) NOT NULL,
  `name` varchar(128) NOT NULL,
  `mime` varchar(128) NOT NULL,
  `size` int(11) NOT NULL,
  `description` text NOT NULL,
  `content` longblob NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `filesOnDisc`
--

DROP TABLE IF EXISTS `filesOnDisc`;
CREATE TABLE `filesOnDisc` (
  `id` bigint(20) NOT NULL,
  `owner` bigint(20) NOT NULL,
  `name` varchar(128) NOT NULL,
  `mime` varchar(128) NOT NULL,
  `size` int(11) NOT NULL,
  `description` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `filesInDB`
--
ALTER TABLE `filesInDB`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `filesOnDisc`
--
ALTER TABLE `filesOnDisc`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `filesInDB`
--
ALTER TABLE `filesInDB`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `filesOnDisc`
--
ALTER TABLE `filesOnDisc`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;COMMIT;
