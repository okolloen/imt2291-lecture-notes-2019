<?php
require_once '../../twig/vendor/autoload.php';
require_once '../classes/DB.php';

$loader = new Twig_Loader_Filesystem('./twig');
$twig = new Twig_Environment($loader, array(
//    'cache' => './compilation_cache',
));

if (!isset($_FILES['fileToUpload'])) {
  echo $twig->render('upload.html', array());
  exit();
}

$db = DB::getDBConnection();

if (is_uploaded_file($_FILES['fileToUpload']['tmp_name'])) {
  $mime = $_FILES['fileToUpload']['type'];
  $name = $_FILES['fileToUpload']['name'];
  $size = $_FILES['fileToUpload']['size'];
  $sql = "INSERT INTO filesOnDisc (owner, name, mime, size, description) VALUES (:owner, :name, :mime, :size, :description)";
  $sth = $db->prepare ($sql);
  $sth->bindParam(':owner', $_POST['uid']);
  $sth->bindParam(':name', $name);
  $sth->bindParam(':mime', $mime);
  $sth->bindParam(':size', $size);
  $sth->bindParam(':description', $_POST['descr']);
  $sth->execute ();
  if ($sth->rowCount()==1) {
    $id = $db->lastInsertId();
    if (!file_exists('uploadedFiles/'.$_POST['uid'])) { // Brukeren har ikke lastet opp filer tidligere
      @mkdir('uploadedFiles/'.$_POST['uid']);
    }
    if (@move_uploaded_file($_FILES['fileToUpload']['tmp_name'], "uploadedFiles/{$_POST['uid']}/$id")) {
      echo $twig->render('uploadSuccess.html', array('fname'=>$name, 'size'=>$size));
    } else {
      $sql = "delete from filesOnDisc where id=$id";
      $db->exec($sql);
      echo $twig->render('uploadFailed.html', array('fname'=>$name, 'msg'=>'Unable to move uploaded file to destination folder.'));
    }
  } else {
    echo $twig->render('uploadFailed.html', array('fname'=>$name));
  }
} else {  // Some trickery is going on
  echo $twig->render('badbadbad.html', array());
}
