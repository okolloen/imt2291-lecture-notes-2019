let flex = {
  "flex-direction": ["row", "row-reverse", "column", "column-reverse"],
  "flex-wrap": ["nowrap", "wrap", "wrap-reverse"],
  "justify-content": ["flex-start", "flex-end", "center", "space-between", "space-around", "space-evenly"],
  "align-items": ["flex-start", "flex-end", "center", "baseline", "stretch"],
  "align-content": ["flex-start", "flex-end", "center", "space-between", "space-around", "stretch"],
};

let flexItem = {
  "order": "Number",
  "flex-grow": "Number",
  "flex-shrink": "Number",
  "align-self": ["auto", "flex-start", "flex-end", "center", "baseline", "stretch"]
}

$(function(){
  $("#effectiveFlexOptions").val(".container {\n\tdisplay: flex;\n}");
  createFlexOptions();      // Create GUI for flex container setup
  $("#flexOptions select").change(flexOptionsChanged);
  createItemOptions();      // Create GUI for flex item setup
  $(".itemSettings select").change(flexItemOptionsChanged);
  $(".itemSettings input").change(flexItemOptionsChanged);
  flexOptionsChanged();   // Set initial CSS
});

function createFlexOptions() {
  let flexOptions = document.getElementById("flexOptions");

  for (option in flex) {
    let div = document.createElement ("DIV");
    div.appendChild(document.createTextNode(option));
    div.appendChild(document.createElement("BR"));
    let select = document.createElement ("SELECT");
    select.setAttribute("id", "flexOptions_"+option.replace(/-/g, "_"));
    let options = flex[option];
    for (let i=0; i<options.length; i++) {
      let option = document.createElement("OPTION");
      option.setAttribute ("value", options[i]);
      if (i==0) {
        option.setAttribute ("selected", "selected");
      }
      option.appendChild(document.createTextNode(options[i]+(i==0?" (default)":"")));
      select.appendChild (option);
    }
    div.appendChild(select);
    flexOptions.appendChild(div);
  }
}

function createItemOptions() {
  let flexItems = document.querySelectorAll(".itemSettings");
  for (let i=0; i<flexItems.length; i++) {
    let item = flexItems[i];
    // item.dataset.item;
    for (option in flexItem) {
      let div = document.createElement("DIV");
      let label = document.createElement("LABEL");
      label.appendChild(document.createTextNode(option));
      div.appendChild(label);
      item.appendChild(div);
      if (flexItem[option]==="Number") {  // Create nummeric input
        let input = document.createElement("INPUT");
        input.setAttribute("type", "number");
        input.setAttribute("min", "0");
        input.setAttribute("max", "100");
        input.dataset.item = item.dataset.item;
        input.dataset.name = option;
        div.appendChild(input);
      } else {  // Create drop down
        let select = document.createElement ("SELECT");
        select.dataset.item = item.dataset.item;
        select.dataset.name = option;
        let options = flexItem[option];
        for (let i=0; i<options.length; i++) {  // Add options to drop down
          let option = document.createElement("OPTION");
          option.setAttribute ("value", options[i]);
          if (i==0) {
            option.setAttribute ("selected", "selected");
          }
          option.appendChild(document.createTextNode(options[i]+(i==0?" (default)":"")));
          select.appendChild (option);
        }
        div.appendChild(select);
      }
    }
  }
}

function flexOptionsChanged() {
  let options = {"display": "flex"};
  let css = ".container {\n\tdisplay: flex;\n";
  css += "\twidth: 600px;\n\theight: 200px;\n\tborder: 1px dashed black;\n";
  $("#flexOptions select").each(function(idx, select) { // Go through all drop down boxes
    if (select.selectedIndex!=0) {    // Not default, show and use
      css += "\t"+select.id.substring(12).replace(/_/g, "-")+": ";
      options[select.id.substring(12).replace(/_/g, "-")] = select.options[select.selectedIndex].text;
      css += select.options[select.selectedIndex].text+";\n";
    } else {        // Default, just use (must override)
      let name = select.id.substring(12).replace(/_/g, "-");
      let val = select.options[select.selectedIndex].text;
      val = val.substring(0, val.indexOf("(")-1);
      options[name] = val;
    }
  });
  css += "}";
  $("#effectiveFlexOptions").val(css);
  $("#demo").css(options);
}

function flexItemOptionsChanged() {
  if (this.children.length>0) { // Drop down box
    $("."+this.dataset.item).css(this.dataset.name, this.options[this.selectedIndex].text);
  } else { // Nummeric input
    $("."+this.dataset.item).css(this.dataset.name, this.value);
  }
}
