import { LitElement, html } from 'lit-element';
import './redux-button.js';

import store from './js/store/index';

class DemoApp extends LitElement {
  static get properties() {
    return {
      technology: { type: String },
      clicks: { type: Number }
    };
  }

  constructor () {
    super();
    const data = store.getState();      // Get state from store
    this.technology = data.technology;
    this.clicks = data.clicks;
    store.subscribe((state)=>{          // When state changes, update locale state
      this.technology = store.getState().technology;
      this.clicks = store.getState().clicks;
    })
  }

  render() {
    const technologies = ['JavaScript', 'Redux', 'litElement', 'Polymer'];
    return html`
      <h1>Hallo</h1>
      ${(this.technology.length>0)?             // If technology is set
        html`<p>${this.technology} er flott<p>`:// Show selected technology
        html``                                  // If not, don't show anything
      }
      ${(this.clicks==0)?
        html`<p>Du har ikke valgt teknologi enda</p>`:
        html`<p>Du har valgt teknologi ${this.clicks} ganger</p>`
      }
      ${technologies.map(t=>{                     // Create one button for each technology
        return html`
          <redux-button technology="${t}"></redux-button>`;
      })}
    `;
  }
}

customElements.define('demo-app', DemoApp);
