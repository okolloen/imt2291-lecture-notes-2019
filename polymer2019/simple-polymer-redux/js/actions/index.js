import { TECHNOLGY_SELECTED } from "../constants/action-types";
import { BUTTON_CLICKED } from "../constants/action-types";

export function selectTechnology(technology) {
  return {
    type: TECHNOLGY_SELECTED,
    chosen: technology
  }
}

export function clicked() {
  return {
    type: BUTTON_CLICKED
  }
}
