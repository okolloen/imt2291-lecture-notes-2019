import { TECHNOLGY_SELECTED, BUTTON_CLICKED } from "../constants/action-types";

const initialState = {
  clicks: 0,
  technology: ""
};

function rootReducer(state = initialState, action) {
  if (action.type === TECHNOLGY_SELECTED) {
    return {
      ...state,
      technology: action.chosen
    }
  } else if (action.type === BUTTON_CLICKED) {
    return {
      technology: state.technology,
      clicks: state.clicks+1            // state.clicks++ does not work, why ??????????
    }
  }
  return state;
};

export default rootReducer;
