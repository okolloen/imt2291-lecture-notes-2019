import { LitElement, html } from 'lit-element';
import store from './js/store/index';
import { selectTechnology, clicked } from './js/actions/index';

class ReduxButton extends LitElement {
  static get properties() {
    return {
      technology: { type: String }
    };
  }

  constructor () {
    super();
  }

  render() {
    return html`
      <button @click="${e=>this.clicked(e)}" data-technology="${this.technology}">${this.technology}</button>
    `;
  }

  clicked(e) {
    store.dispatch(selectTechnology(e.target.dataset.technology));
    store.dispatch(clicked());
  }
}

customElements.define('redux-button', ReduxButton);
