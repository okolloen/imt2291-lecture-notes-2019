import { PolymerElement, html } from '@polymer/polymer/polymer-element.js';
import './shared-styles.js';

class AdminView1 extends PolymerElement {
  sendFile(e) {
    e.preventDefault();
    const file = e.target.form.file.files[0];
    const progress = e.target.form.querySelector('progress');
    progress.style.display = 'block';

    const xhr = new XMLHttpRequest();
    xhr.file = file; // not necessary if you create scopes like this
    xhr.addEventListener('progress', function(e) {
      const done = e.position || e.loaded, total = e.totalSize || e.total;
      progress.value = (Math.floor(done/total*1000)/10);
      console.log('xhr progress: ' + (Math.floor(done/total*100)) + '%');
    }, false);
    if ( xhr.upload ) {
      xhr.upload.onprogress = function(e) {
        const done = e.position || e.loaded, total = e.totalSize || e.total;
        progress.value = (Math.floor(done/total*100));
        console.log('xhr.upload progress: ' + done + ' / ' + total + ' = ' + (Math.floor(done/total*1000)/10) + '%');
      };
    }
    xhr.onreadystatechange = function(e) {
      if ( 4 == this.readyState ) {
        progress.style.display = 'none';
        const res = JSON.parse(e.target.response);
        console.log(['xhr upload complete', e, res]);
        // File transfer successfull
      }
    };
    xhr.withCredentials = true;    // Needed to send the session id cookie
    xhr.open('post', `${window.MyAppGlobals.serverURL}api/uploadProgress.php`, true);
    xhr.withCredentials = true;
    xhr.setRequestHeader("Content-Type", "application/octet-stream");
    xhr.setRequestHeader("X-OriginalFilename", file.name);
    xhr.setRequestHeader("X-OriginalFilesize", file.size);
    xhr.setRequestHeader("X-OriginalMimetype", file.type);
    // Add any other information about the file to be transfered as X-......
    xhr.send(file);
  }

  static get template() {
    return html`
      <style include="shared-styles">
        :host {
          display: block;

          padding: 10px;
        }
        progress {
          display: none;
          width: 20em;
        }
      </style>

      <div class="card">
        <div class="circle">1</div>
        <h1>Admin modul 1</h1>
        <form method="post" enctype="multipart/form-data">
          <input type="file" name="file">
          <button on-click="sendFile">Send fil</button><br/>
          <progress value="0" max="100" name="progress"></progress>
        </form>
      </div>
    `;
  }

  firstUpdated(changedProperties) {
    this._root.querySelector('button').addEventListener('click', this.send);
  }
}

customElements.define('admin-view1', AdminView1);
