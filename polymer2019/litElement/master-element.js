import { LitElement, html, css } from 'lit-element';
import './first-element.js';
import './second-element.js';


class MasterElement extends LitElement {
  static get properties() {
    return {
      prop1: { type: String },
      prop2: { type: Number }
    }
  }

  constructor() {
    super();
    this.prop1 = 'Hello World';
    this.prop2 = 42;
    this.addEventListener('textUpdated', e => this.prop1 = e.detail.string);
    this.addEventListener('numberUpdated', e => this.prop2 = e.detail.number);
  }

  render() {
    return html`
      <style>
        :host {
          display: block;
          font-size: 2em;
        }

        :host>div {
          width:800px;
          display: flex;
          flex-direction: row;
        }
        div>div, .d {
          padding: 15px;
          flex-grow:1;
          border: 1px solid grey;
        }
      </style>
      <div>
        <div><first-element string="${this.prop1}" number="${this.prop2}"></first-element></div>
        <div><second-element string="${this.prop1}" number="${this.prop2}"></second-element></div>
      </div>
    `;
  }
}

customElements.define('master-element', MasterElement);
