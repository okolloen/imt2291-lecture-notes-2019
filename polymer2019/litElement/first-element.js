import { LitElement, html } from 'lit-element';

class FirstElement extends LitElement {
  static get properties() {
    return {
      string: { type: String },
      number: { type: Number }
    };
  }

  constructor() {
    super();
    this.string = 'Hello World';
    this.number = 5;
  }

  render() {
    return html`
      <style>
        input {
          font-size: 1em;
        }
      </style>
      <p><input value="${this.string}" @input=${(e) => this.newText(e.target.value)}</p>
      <p>number: ${this.number}</p>
    `;
  }

  updated(changedProperties) {
    changedProperties.forEach((oldValue, propName) => {
      console.log(`${propName} changed. oldValue: ${oldValue}`);
    });
  }

  newText(string) {
    this.dispatchEvent(new CustomEvent("textUpdated", {
      bubbles: true,
      composed: true,
      detail:{
        string:string
      }
    }));
  }
}

customElements.define('first-element', FirstElement);
