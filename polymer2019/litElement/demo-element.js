import { LitElement, html } from 'lit-element';

class DemoElement extends LitElement {
  static get properties() { return {
    prop1: { type: String },
    prop2: { type: Number },
    prop3: { type: Boolean,
              converter: {  // A boolean attribute that exists is intepreted to be true
                fromAttribute(value) {  // Change converter for this attribute
                  if (value==null)      // If it does not exist
                    return false;
                  if (value=='false')   // or is false, return false
                    return false;
                  return true;
                }
              }},
    prop4: { type: Array },
    prop5: { type: Object }
  };}

  constructor() {
    super();
    this.prop1 = 'Hello World';
    this.prop2 = 5;
    this.prop3 = false;
    this.prop4 = [1,2,3];
    this.prop5 = { subprop1: 'prop 5 subprop1 value' }
  }

  render() {
    return html`
      <p>prop1: <input value="${this.prop1}" @input=${(e) => this.setAttribute('prop1',e.target.value)}</p>
      <p>prop2: ${this.prop2}</p>
      <p>prop3: ${this.prop3}</p>
      <p>prop4: <ul>${this.prop4.map(e=>{
        return html`<li>${e}</li>`;
      })}</ul></p>
      <p>prop5.subprop1: ${this.prop5.subprop1}</p>
      <button @click="${this.changeProperties}">change properties</button>
      <button @click="${this.changeAttributes}">change attributes</button>
    `;
  }

  changeAttributes() {
    let randy = Math.floor(Math.random()*10);
    let myBool = this.prop3;

    this.setAttribute('prop1', randy.toString);
    this.setAttribute('prop2', randy);
    this.setAttribute('prop3', myBool? 'false' : 'true');
    this.setAttribute('prop4',
      JSON.stringify(Object.assign([], [...this.prop4], randy)));
    this.setAttribute('prop5',
      JSON.stringify(Object.assign({}, this.prop5, {[randy]: randy})));
    this.requestUpdate();
  }

  changeProperties() {
    let randy = Math.floor(Math.random()*10);
    let myBool = this.prop3;

    this.prop1 = randy.toString();
    this.prop2 = randy;
    this.prop3 = !myBool;
    this.prop4 = Object.assign([], [...this.prop4], randy);
    this.prop5 = Object.assign({}, this.prop5, {[randy]: randy});
  }

  updated(changedProperties) {
    changedProperties.forEach((oldValue, propName) => {
      console.log(`${propName} changed. oldValue: ${oldValue}`);
    });
  }
}

customElements.define('demo-element', DemoElement);
