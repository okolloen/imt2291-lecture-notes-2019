import { LitElement, html } from 'lit-element';

class SecondElement extends LitElement {
  static get properties() {
    return {
      string: { type: String },
      number: { type: Number }
    };
  }

  constructor() {
    super();
    this.string = 'Hello World';
    this.number = 5;
  }

  render() {
    return html`
      <h1>${this.string}</h1>
      <input type="range" min="1" max="100" value="${this.number}"
      @change="${e=>this.newValue(e.target.value)}">
    `;
  }

  updated(changedProperties) {
    changedProperties.forEach((oldValue, propName) => {
      console.log(`${propName} changed. oldValue: ${oldValue}`);
    });
  }

  newValue(value) {
    this.dispatchEvent(new CustomEvent("numberUpdated", {
      bubbles: true,
      composed: true,
      detail:{
        number:value
      }
    }));
  }
}

customElements.define('second-element', SecondElement);
