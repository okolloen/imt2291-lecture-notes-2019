  let section = document.getElementById('jsonData');
  fetch('js/menu.json')
  .then(res=>res.json())
  .then(data=>{
    let header = document.createElement('H3');
    header.innerHTML = 'Menyen inneholder følgende punkter';
    section.appendChild(header);
    let liste = document.createElement('OL');
    section.appendChild(liste);
    data.forEach(menuItem=>{
      let li = document.createElement('LI');
      liste.appendChild(li);
      let ul = document.createElement('UL');
      li.appendChild(ul);
      ul.innerHTML = `<li>MenyTekst -> ${menuItem.html}</li>
      <li>Section ID -> ${menuItem.id}</li>
      <li>JavaScript URL -> ${menuItem.scriptsrc}</li>`;
    });
  });
