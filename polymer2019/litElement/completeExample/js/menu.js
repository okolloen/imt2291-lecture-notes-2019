fetch('js/menu.json')
.then(res=>res.json())
.then(data=>{
  let menu = document.querySelector('nav ul');
  data.forEach((menuItem, idx)=>{
    let li = document.createElement('li');
    let active = '';
    if (idx==0) {
      active = ' class="active"';
    }
    li.innerHTML = `<a data-id="${menuItem.id}" data-scriptsrc="${menuItem.scriptsrc}" href=""${active}>${menuItem.html}</a>`;
    menu.appendChild(li);
  });

  document.querySelectorAll('nav a').forEach(menuItem=>{
    menuItem.addEventListener('click', e=>{
      if (e.target.dataset.scriptsrc!="") {
        if (document.querySelector(`[src="${e.target.dataset.scriptsrc}"]`)==null) {
          let script = document.createElement('SCRIPT');
          script.src = e.target.dataset.scriptsrc;
          document.querySelector('head').appendChild(script);
        }
      }
      document.querySelectorAll('body>section>section').forEach(section=>{
        if (section.id==e.target.dataset.id) {
          section.classList.add('active');
        } else {
          section.classList.remove('active');
        }
      });

      document.querySelectorAll('nav>ul>li>a').forEach(menuItem=>{
        if (menuItem!=e.target) {
          menuItem.classList.remove('active');
        } else {
          menuItem.classList.add('active');
        }
      });
      e.preventDefault();
    });
  });
});
