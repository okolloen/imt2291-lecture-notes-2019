document.querySelector('[name="C"]').addEventListener('input', e=>{
  let F = e.target.value* 9/5 + 32;
  if (isNaN(F)) {
    e.target.form.F.value = "Vær snill å skrive inn tall";
  } else {
    e.target.form.F.value = F.toFixed(2);
  }
});
