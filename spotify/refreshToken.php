<?php
session_start();

$url = 'https://accounts.spotify.com/api/token';
$data = array('grant_type' => 'refresh_token',
              'refresh_token' => $_SESSION['refresh_token']);

// use key 'http' even if you send the request to https://...
$options = array(
    'http' => array(
        'header'  => "Content-type: application/x-www-form-urlencoded\r\n",
        'method'  => 'POST',
        'content' => http_build_query($data)
    )
);
$context  = stream_context_create($options);
$result = file_get_contents($url, false, $context);
if ($result === FALSE) {
  die (json_encode (array('failed'=>'misserably')));
 }

$data = json_decode($result, true);
$_SESSION['access_token'] = $data['access_token'];
$_SESSION['token_type'] = $data['token_type'];
if (isset($data['refresh_token'])) {
  $_SESSION['refresh_token'] = $data['refresh_token'];
}
$_SESSION['scope'] = $data['scope'];

echo json_encode (array('access_token' => $_SESSION['access_token'],
                          'token_type' => $_SESSION['token_type'],
                          'refresh_token' => $_SESSION['refresh_token'],
                          'scope' => $_SESSION['scope']));
