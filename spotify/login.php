<?php
$clientID = '41f7402c6ec84a56a3ab7547e98cfc53';
$scopes = 'user-read-private user-read-email';
$redirectURI = 'http://localhost/imt2291/spotify/callback.php';

header ('location: https://accounts.spotify.com/authorize' .
  '?response_type=code' .
  '&client_id=' . $clientID .
  (isset($scopes) ? '&scope=' . urlencode($scopes) : '') .
  '&redirect_uri=' . urlencode($redirectURI));
