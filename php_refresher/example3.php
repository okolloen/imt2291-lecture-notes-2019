<?php
/**
 * This is a simple class that shows constructors, destructors
 * and the toString method.
 *
 * @author Øivind Kolloen, roughly a translation of code from https://code.tutsplus.com/tutorials/object-oriented-php-for-beginners--net-12762
 *
 */
class MyClass {
  /**
   * A public variable with no real functionality
   * @var string stores a simple string
   */
  public $prop1 = 'Jeg er en egenskap i klassen';

  /**
   * The constructor is called when a new object of this class is constructed.
   */
  public function __construct() {
    echo 'Et objekt av  klassen "', __CLASS__, '" er opprettet!<br />';
  }

  /**
   * The desctructor is called when the object is removed/destroyed.
   */
  public function __destruct() {
    echo 'Objektet av klassen "', __CLASS__, '" er fjernet.<br />';
  }

  /**
   * The toString method is called when this object is converted into a string.
   */
  public function __toString() {
    echo 'Bruker toString metoden: ';
    return $this->getProperty();
  }

  /**
   * Method used to set the value of the single property of this object.
   *
   * @param string the new value of the property.
   */
  public function setProperty($newval) {
    $this->prop1 = $newval;
  }

  /**
   * Method used to get the value of the single property of this object.
   *
   * @return string the value of the property.
   */
  public function getProperty() {
    return $this->prop1 . '<br />';
  }
}

// Create a new object
$obj = new MyClass;

// Output the object as a string
echo $obj;

echo '<pre>';
var_dump ($obj);
print_r ($obj);
echo '</pre>';

// Destroy the object
// unset($obj);

// Output a message at the end of the file
echo 'Slutten på filen.<br />';
